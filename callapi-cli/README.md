# Callapi-cli

## What is callapi-cli?

Callapi-cli is a simple cli program that fetches data from an API,  
and outputs a data.csv file of the fetched data.  

## How to get started?

### Setup rust

Go to [[rustup]](https://rustup.rs/) and follow the instillation instructions.

### Build the program

Make sure you're in the root directory, and run  

```
cargo build  
```

### Run the program

Make sure you're in the root directory, and run  

```
cargo run  
```

Alternatively, you can execute the binary directly  

```
cd /target/debug/  

./callapi-cli  
```

## Where to find the outputted csv file?

Callapi-cli uses [[XDG Base Directoy specifications]](https://wiki.archlinux.org/title/XDG_Base_Directory)  

You can find the 'data.csv' file, in your XDG_DATA_HOME.  
