use csv::Writer;
use reqwest::header::{HeaderMap, ACCEPT, AUTHORIZATION};
use std::fs::OpenOptions;

fn main() {
    let apikey: &str
        = "apikey eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlRDhaeVNRM0pGSnNmd2xsaGFYZi03TmxzT09BaDhpanFvXzRKMHlQNmdnIiwiaWF0IjoxNjk0NzM5Mjc1fQ.BpPWKYyiGlkTpRci1bYxAl5_YJw0tPAyXJ4csECObuY";
    let limit: i32 = 1000;
    let request_url = format!(
        "https://api.transport.nsw.gov.au/v1/maritime/spatial?format=csv&q=select%20*%20from%20boat_ramps%20limit%20{:?}%20", limit);

    let xdg_dirs = xdg::BaseDirectories::with_prefix("onepiece").unwrap();

    let mut headers = HeaderMap::new();
    headers.insert(ACCEPT, "csv".parse().unwrap());
    headers.insert(AUTHORIZATION, apikey.parse().unwrap());

    let file = OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(
            xdg_dirs
                .place_data_file("data.csv")
                .expect("couldn't create csv file"),
        )
        .unwrap();

    let mut writer = Writer::from_writer(file);
    let client = reqwest::blocking::Client::new();

    match client.get(request_url).headers(headers).send() {
        Ok(resp) => writer.serialize(resp.text().unwrap()).expect("success"),
        Err(err) => panic!("Error: {}", err),
    };
}
